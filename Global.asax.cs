﻿using System;
using System.Web.Http;

namespace Gin.FTS
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start(Object sender, EventArgs e)
        {
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(UnityConfig.Container);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
