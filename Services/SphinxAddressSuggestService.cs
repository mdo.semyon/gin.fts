﻿using System.Collections.Generic;
using System.Linq;
using Gin.Business.Services.Address;
using Gin.FTS.Helpers;
using Mdo.Common;
using Mdo.Sql;
using Mdo.Sql.Base;
using Mdo.Sql.Exceptions;
using Mdo.Sql.MySql.Providers;

namespace Gin.FTS.Services
{
    public class SphinxAddressSuggestService : IAddressSuggestService
    {
        private readonly string _sphinxConnectionString;

        public SphinxAddressSuggestService(string sphinxConnectionString)
        {
            _sphinxConnectionString = sphinxConnectionString;
        }

        private static readonly AddressData Moscow = new AddressData
        {
            fias_id = "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
            fias_level = "1",
            kladr_id = "7700000000000",
            region_fias_id = "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
            region = "Москва",
            region_type = "г",
            region_type_full = "город",
            region_with_type = "г Москва"
        };

        private const string SearchSql =
            "SELECT id, weight() as weight, "+
            "fullname, postal_code, fias_level, region_fias_id, region, region_type, region_type_full, region_with_type, area_fias_id, area, area_type, area_type_full, area_with_type, city_fias_id, city, city_type, city_type_full, city_with_type, city_district_fias_id, city_district, city_district_type, city_district_type_full, city_district_with_type, settlement_fias_id, settlement, settlement_type, settlement_type_full, settlement_with_type, street_fias_id, street, street_type, street_type_full, street_with_type, house_fias_id, house, house_type, house_type_full, block, block_type, block_type_full " +
            "FROM fias_united WHERE MATCH(?p1) ORDER BY weight DESC LIMIT 0, ?p2 OPTION ranker=expr('sum(3*lcs+6*atc+exact_hit)*1000+500*item_weight+bm25'), cutoff=15000, idf='plain,tfidf_unnormalized'";

        private static IEnumerable<T> DbQuery<T>(ISqlDataProvider ctx, string sql, params object[] queryParams)
        {
            var data = ctx.SqlReturnDataset(sql, queryParams);
            if (ctx.WasError)
                throw new QueryExecutionException(sql, data);

            return SqlMapper.Map<T>(data.ResultData);
        }

        public SuggestAddressResponse Search(AddressSuggestQuery query)
        {
            if (string.IsNullOrWhiteSpace(query.query))
                return new SuggestAddressResponse {suggestions = new List<SuggestAddressResponse.Suggestions>()};
            if (query.count > 15)
                query.count = 15;

            IEnumerable<SuggestAddressResponse.Suggestions> suggests = new SuggestAddressResponse.Suggestions[0];
            query.query = query.query.EscapeSphinxQL();

            using (ISqlDataProvider sphCtx = new SphinxSqlDataProvider(_sphinxConnectionString))
            {
                var loaded = DbQuery<AddressSuggestion>(sphCtx, SearchSql,
                    $"(\"{query.query}*\"~4) | (=\"{query.query}\")", query.count).ToList();

                suggests = loaded
                    .OrderByDescending(x => x.Weight)
                    .ThenBy(x => x.fullname, AlphanumComparer.Instance)
                    .Select(x => new SuggestAddressResponse.Suggestions
                {
                    data = x,
                    value = x.fullname,
                    item_weight = x.item_weight,
                    weight = x.Weight
                });
            }
            return new SuggestAddressResponse
            {
                suggestions = suggests.Take(query.count)
            };
        }

        public AddressStatusResponse GetStatus()
        {
            return new AddressStatusResponse
            {
                enrich = false,
                name = "address",
                search = true,
                state = "ENABLED",
                resources = new[]
                    {
                        new AddressStatusResponse.Resource
                        {
                            name = "fias",
                            version = "1.0",
                            count = 25000000
                        }
                    }
            };
        }

        public AddressByIpResponse DetectAddressByIp(string ip)
        {
            return new AddressByIpResponse
            {
                location = new AddressByIpResponse.Location
                {
                    data = Moscow,
                    value = ip 
                }
            };
        }
    }
}
