﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Gin.Business.Services.Address;
using Gin.FTS.Helpers;

namespace Gin.FTS.Controllers
{
    public class AddressSuggestController : ApiController
    {
        private readonly IAddressSuggestService _addressSuggestService;

        public AddressSuggestController(IAddressSuggestService addressSuggestService)
        {
            _addressSuggestService = addressSuggestService;
        }

        [HttpPost]
        public HttpResponseMessage Suggest(AddressSuggestQuery q)
        {
            var result = _addressSuggestService.Search(q);
            return CreateHttpResponseMessage(result);
        }

        [HttpGet]
        public HttpResponseMessage Status()
        {
            var result = _addressSuggestService.GetStatus();
            return CreateHttpResponseMessage(result);
        }

        [HttpGet]
        public HttpResponseMessage DetectAddressByIp()
        {
            var result = _addressSuggestService.DetectAddressByIp(Request.GetClientIp());
            return CreateHttpResponseMessage(result);
        }

        private HttpResponseMessage CreateHttpResponseMessage(object result)
        {
            return Request.CreateResponse(
                result != null ? HttpStatusCode.OK : HttpStatusCode.InternalServerError,
                result ?? new {error = "Ошибка при получении значений!"},
                "application/json");
        }
    }
}
