using System;
using System.Configuration;
using Gin.FTS.Services;
using Gin.Business.Services.Address;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Injection;

namespace Gin.FTS
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> ContainerFactory =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => ContainerFactory.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IAddressSuggestService, SphinxAddressSuggestService>(new PerRequestLifetimeManager(), new InjectionConstructor(ConfigurationManager.ConnectionStrings["Sphinx"].ConnectionString));
        }
    }
}