﻿using System;
using System.Linq;
using System.Web;

namespace Gin.FTS.Helpers
{
    public static class RequestExtension
    {
        public static string GetClientIp(this HttpRequestBase request)
        {
            var ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(ip))
            {
                ip = ip.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
                if (!string.IsNullOrEmpty(ip))
                    return ip;
            }
            return request.UserHostAddress;
        }

        public static string GetClientIp(this System.Net.Http.HttpRequestMessage request)
        {
            const string httpContext = "MS_HttpContext";
           
            if (request.Properties.ContainsKey(httpContext))
                return ((HttpContextWrapper)request.Properties[httpContext]).Request.GetClientIp();

            return string.Empty;
        }
    }
}