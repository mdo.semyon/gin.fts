﻿using System.Linq;
using System.Text;

namespace Gin.FTS.Helpers
{
    internal static class StringExtension
    {
        private static readonly char[] SpecialChars = { '\\', '(', ')', '|', '-', '!', '@', '~', '"', '&', '/', '^', '$', '=', '\'', '\x00', '\n', '\r', '\x1a' };

        public static string EscapeSphinxQL(this string s)
        {
            var sb = new StringBuilder();
            foreach (var c in s)
            {
                if (SpecialChars.Contains(c))
                    sb.Append('\\');
                sb.Append(c);
            }
            return sb.Length != s.Length ? sb.ToString() : s;
        }
    }
}
